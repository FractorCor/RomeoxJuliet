# RomeoxJuliet

This project serves a playground for me to establish best practices and other
development techiques for working in Ren'py. To guide development, the script of
['Romeo and Juliet'][RxJScript] is used to guide the project by addressisng some
of the creative work that needs to be done.

## TODO

- [ ] Asset Pipeline
  - [ ] Writing
  - [ ] Drawing
    - [ ] Character Sprites
    - [ ] UI
    - [ ] Mini-CGs
- [ ] Sprite Display
  - [ ] Transform
    - [ ] Positioning
  - [ ] Animation
    - [ ] Facial expressions
    - [ ] Status effects (waterdropm, sweat, emojis essentially)
- [ ] Scenes
  - [ ] Transitions
  - [ ] Interludes
- [ ] Explore alternative GUIs
  - [ ] NVL
  - [ ] Dialogue Box w/ integrated namebox
  - [ ] Dialogue Box w/ discrete namebox
- [ ] Spruce up menus
  - [ ] Main Menu
  - [ ] Save/Load Menu
  - [ ] Settings
- [ ] Asset Collection
  - [ ] CG Playback
  - [ ] Scene Playback
  - [ ] Audio Playback
- [ ] Credits Scene

## License

This project is licensed under the MIT License (Expat). See LICENSE file for
details.

[RxJScript]:http://shakespeare.mit.edu/romeo_juliet/full.html
