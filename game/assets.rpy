# Declare assets (e.g. images, sounds) used by this game.

init -1:

    # Paths to assets stored on disk.
    python:
        # Music Tracks
        music_prologue = "music/the-forgotten-age.mp3"

        # Ambient Tracks
        ambient_neutral = "sounds/ambience/outdoors_neutral.flac"

    # Images
    image logo fractorcor = "images/fractor-cor-logo.png"

    image black = Solid((0, 0, 0, 255))
    image white = Solid((255, 255, 255, 255))
    image grey  = Solid((128, 128, 128, 255))

    #Characters
    image sampson = "images/characters/sampson.png"
    image gregory = "images/characters/gregory.png"
