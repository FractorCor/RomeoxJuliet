# Declare characters used by this game.

init -1:
    define n         = Character(None, kind=nvl)
    define sampson   = Character('Sampson', show_two_window=True)
    define gregory   = Character('Gregory', show_two_window=True)
    define abraham   = Character('Abraham', show_two_window=True)
    define balthasar = Character('Balthasar', show_two_window=True)
    define benvolio  = Character('Benvolio', show_two_window=True)
    define tybalt    = Character('Tybalt', show_two_window=True)
    define ladyMont  = Character('Lady Montague', show_two_window=True)
    define dudeMont  = Character('Montague', show_two_window=True)
    define ladyCapu  = Character('Lady Capulet', show_two_window=True)
    define dudeCapu  = Character('Capulet', show_two_window=True)

    define firstciti = Character('First Citizen', show_two_window=True)
    define prince    = Character('Prince', show_two_window=True)

    define romeo     = Character('Romeo', show_two_window=True)
    define juliet    = Character('Juliet', show_two_window=True)
