"Ren'Py" by [Tom Rothamel][PyTom] is licensed under [MIT License][MIT]
"RPG UI" by [Wyrmheart][Wyrmheart] is Public Domain

[PyTom]:https://github.com/renpytom
[MIT]:https://opensource.org/licenses/MIT
[Wyrmheart]:http://opengameart.org/content/ui-resources
