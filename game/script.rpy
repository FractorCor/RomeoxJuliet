﻿# You can place the script of your game in this file.

# Scripting Conventions
# - no lines longer than 80 columns
# - named individuals are to use their first name (+ last init, if conflict)
# - non-descript characters are to use a shortened name, like 'n' for narration

# Scripting Guidelines
# - apply transitions when rearranging or introducing another sprite
#   - this could also apply for changing stances or facial expressions
#       - don't change poses/facial expresions until their next speaking moment
# - apply screenwipes to provide sense of direction when switching locations

label splashscreen:
    scene white
    with Pause(1)

    show logo fractorcor at truecenter
    with dissolve
    with Pause(2)

    scene black with dissolve
    with Pause(1)

    return

# The game starts here.
label start:
    jump actIprologue

label actIprologue:

    play music music_prologue

    n "In two households, both alike in dignity,"
    n "In fair Verona, where we lay our scene,"
    n "From ancient grudge break to new mutiny,"
    n "Where civil blood makes civil hands unclean.\n"

    n "From forth the fatal loins of these two foes"
    n "A pair of star-cross'd lovers take their life;\n"

    n "Whose misadventured piteous overthrows"
    n "Do with their death bury their parents' strife.\n"

    n "The fearful passage of their death-mark'd love,"
    n "And the continuance of their parents' rage,"
    n "Which, but their children's end, nought could remove,"
    n "Is now the two hours' traffic of our stage;"
    n "The which if you with patient ears attend,"
    n "What here shall miss, our toil shall strive to mind."

    stop music fadeout 1.0

    jump actIsceneI

label actIsceneI:

    play ambience ambient_neutral

    show gregory at center_left
    show sampson at center_right

    sampson "Gregory, o' my word, we'll not carry coals."
    gregory "No, for then we should be colliers."
    sampson "I mean, an we be in choler, we'll draw."
    gregory "Ay, while you live, draw your neck out o' the collar."
    sampson "I strike quickly, being moved."
    gregory "A dog of the house of Montague moves me."
    sampson "To move is to stir; and to be valiant is to stand: therefore if,\
             thou art moved, thou runn'st away."
    gregory "That shows thee a weak slave; for the weakest goes to the wall."
    sampson "True; and therefore women, being the weaker vessels, are ever\
             thrust to the wall: therefore I will push Montague's men from the\
             wall, and thrust his maids to the wall."
    gregory "The quarrel is between our masters and us their men."
    sampson "'Tis all one, I will show myself a tyrant: when I have fought with\
             the men, I will be cruel with the maids, and cut off their heads."
    gregory "The heads of the maids?"
    sampson "Ay, the heads of the maids, or their maidenheads; take it in what\
             sense thou wilt."
    gregory "They must take it in sense that feel it."
    sampson "Me they shall feel while I am able to stand: and 'tis known I am \
             a pretty piece of flesh."
    gregory "'This well thou art not fish; if thou hadst, thou hadst been poor\
             John. Draw they tool! Here comes two of the house of the Montagues."
    sampson "My naked weapon is out: quarrel, I will back thee."
    gregory "How Turn thy back and run?"
    sampson "Fear me not."
    gregory "No, marry; I fear thee!"
    sampson "Let us take the law of our sides; let them begin."
    gregory "I will frown as I pass by, and let them take it as they list."
    sampson "Nay, as they dare. I will bite my thumb at them; which is a\
             disgrace to them, if they bear it."
    gregory "I will frown as I pass by, and let them take it as they list."
    sampson "Nay, as they dare. I will bite my thumb at them; which is a\
             disgrace to them, if they bear it."

    # Enter ABRAHAM and BALTHASAR

    hide gregory
    hide sampson

    abraham "Do you bite your thumb at us, sir?"
    sampson "I do bite my thumb, sir."
    abraham "Do you bite your thumb at us, sir?"
    # Sampson is to [Aside to GREGORY]
    sampson "Is the law of our side, if I say ay?"
    gregory "No."
    sampson "No, sir, I do not bite my thumb at you, sir, but I bite my thumb,\
             sir."
    gregory "Do you quarrel, sir?"
    abraham "Quarrel sir! No, sir."
    sampson "If you do, sir, I am for you: I serve as good a man as you."
    abraham "No better."
    sampson "Well, sir."
    gregory "Say 'better': here comes one of my master's kinsmen."
    sampson "Yes, better, sir."
    abraham "You lie."
    sampson "Draw, if you be men. Gregory, remember thy swashing blow."

    # They fight!
    # Enter BENVOLIO

    benvolio "Part, fools! Put up your swords; you know not what you do."

    # Benvolio beats down their swords
    # Enter TYBALT

    tybalt "What, art thou drawn among these heartless hinds? Turn thee,\
            Benvolio, look upon thy death."
    benvolio "I do but keep the peace: put up thy sword, or manage it to part\
              these men with me."
    tybalt "What, drawn, and talk of peace! I hate the word, as I hate hell,\
            all Montagues, and thee: have at thee, coward!"

    # They fight
    # Enter, several of both houses, who join the fray;
    # then enter Citizens with clubs

    "First Citizen" "Clubs, bills, and partisans! Strike! Beat them down!\
                     Down with the Capulets! Down with the Montagues!"

    # Enter CAPULET in his gown and LADY CAPULET

    dudeCapu "What noise is this? Give me my long sword, ho!"
    ladyCapu "A crutch, a crutch! Why call you for a sword?"
    dudeCapu "My sword, I say! Old Montague is come, and flourishes his blade \
              in spite of me."

    # Enter MONTAGUE and LADY MONTAGUE

    dudeMont "Thou villain Capulet, hold me not, let me go."
    ladyMont "Thou shalt not stir a foot to seek a foe."
    prince "Rebellious subjects, enemies to peace,\
            Profaners of this neighbor-stained steel,--\
            That quench the fire of your pernicious rage\
            With purple fountains issuing from your veins,\
            On pain of torture, from those bloody hands,\
            Throw your mistemper'd weapons to the ground,\
            And hear the sentence of your moved prince.\
            Three civil brawls, bred of an airy word,\
            By thee, old Capulet, and Montague,\
            Have thrice disturb'd the quiet of our streets,\
            And made Verona's ancient citizens\
            Cast by their grave beseeming ornaments,\
            To wield old partisans, in hands as old,\
            Canker'd with peace, to part your canker'd hate:\
            If you ever disturb our streets again,\
            Your lives shall pay the forfeit of the peace.\
            For this time, all the rest depart away:\
            You Capulet; shall go along with me:\
            And Montague, come you this afternoon,\
            To know our further pleasure cause in this case,\
            To old Free-town, our common judgement-place.\
            Once more, on pain of death, all men depart."

    # Exeunt all but MONTAGUE, LADY MONTAGUE, and BENVOLIO

    dudeMont "Who set this ancient quarrel new abroach? Speak, nephew, were you\
              by when it began?"

    return
